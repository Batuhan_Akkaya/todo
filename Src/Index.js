import React, {Component} from 'react';
import {View, StatusBar} from 'react-native';
import Router from './Config/Router';

class Index extends Component {
    render() {
        return (
            <View style={{flex: 1}}>
                <StatusBar backgroundColor='#5096C3' barStyle='light-content'/>
                <Router/>
            </View>
        );
    }
}

export default Index;