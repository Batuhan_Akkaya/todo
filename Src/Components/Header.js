import React from 'react';
import {StyleSheet, View } from 'react-native';
import { Header } from 'react-navigation' ;
import LinearGradient from 'react-native-linear-gradient';

const CustomHeader = props => (
    <View smyle={{ backgroundColor: '#eee' }}>
        <LinearGradient
            colors={['#57abdb', '#31efff']}
            style={[StyleSheet.absoluteFill]}
            start={{x: 0.0, y: 0.0}} end={{x: 1.0, y: 1.0}}
        />
        <Header {...props} style={{ backgroundColor: 'transparent' }}/>
    </View>
);

export default CustomHeader;