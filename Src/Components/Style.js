export default {
    card: {
        flexDirection: 'row',
        borderRadius: 10,
        margin: 4,
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 4,
        elevation: 1,
        padding: 20
    },
    floating: {
        backgroundColor: '#57abdb',
        height: 70,
        width: 70,
        borderRadius: 35,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 20,
        right:20,
        shadowColor: "#000",
        shadowOpacity: 0.3,
        shadowRadius: 1,
        shadowOffset: {height: 2, width: 0},
        elevation: 2
    }
}