import mobx, {observable, action} from 'mobx';

class Store {
    @observable tasks = [];
    @observable done = [];
    @observable adVisible = true;

    @action newTask(task) {
        this.tasks.push(task);
    }
    @action newDone(done) {
        this.done.push(done);
    }
    @action setAdVisible(val) {
        this.adVisible = val;
    }
}

const taskStore = new Store();
export default taskStore;