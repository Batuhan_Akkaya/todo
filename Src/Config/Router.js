import React from 'react';
import {StackNavigator, TabNavigator} from 'react-navigation';

import Home from '../Screens/Home';
import Done from '../Screens/Done'

const TabRoute = TabNavigator({
    Tasks: {screen: Home},
    done: {screen: Done},
}, {
    tabBarOptions: {
        style: {backgroundColor: '#57abdb', shadowOpacity: 0,
            shadowOffset: {
                height: 0,
            },
            shadowRadius: 0, borderWidth: 0},
        labelStyle: {color: '#fff', fontSize: 13, justifyContent: 'center', alignItems: 'center', marginBottom: 15},
    },
    swipeEnabled: true,
    tabBarPosition: 'top',
    animationEnabled: true
});

const StackRoute = StackNavigator({
    App: {screen: TabRoute}
}, {
    navigationOptions: {
        title: 'Todo App',
        headerTintColor: '#fff',
        headerStyle: {backgroundColor: '#57abdb', elevation: 0},
    }
});

export default StackRoute;