import React, {Component} from 'react';
import {Root, View, Content, Text, Left, Right, CheckBox, Icon, ActionSheet, Button} from 'native-base';
import Style from '../Components/Style';
import {observer} from 'mobx-react';
import Store from '../Config/Store';
import {AdMobBanner} from 'react-native-admob';

@observer
class Done extends Component {
    static navigationOptions = {
        tabBarLabel: 'Done',
        headerRight: (<Button transparent onPress={() => new Done().renderActionSheet()}><Icon style={{color: '#fff', marginRight: 16}} name='md-more'/></Button>)
    };

    constructor(props) {
        super(props);
        this.state = {
            clicked: ''
        }
    }

    componentWillMount() {
        const initial_data = [{description: '8 Lorem ipsum dolar'},
            {description: '9 Lorem ipsum dolar'},
            {description: '10 Lorem ipsum dolar'},
            {description: '11 Lorem ipsum dolar'}
        ];
        for (let i = 0; i < initial_data.length; i++) {
            Store.newDone(initial_data[i]);
        }
    }

    renderActionSheet() {
        let BUTTONS = ["Remove Ads", "Show Ads", "Cancel"],
            CANCEL_INDEX = 4;
        ActionSheet.show(
            {
                options: BUTTONS,
                cancelButtonIndex: CANCEL_INDEX,
                title: "Menu"
            },
            buttonIndex => {
                if(parseInt(buttonIndex) === 0)
                    Store.setAdVisible(false);
                else
                    Store.setAdVisible(true);
            }
        )
    }

    check(index) {
        let newTask = Store.done.splice(index, 1)[0];
        Store.newTask(newTask);
    }

    bannerError(e) {
        alert(e);
    }

    renderListItem(item, index) {
        return (
            <View style={Style.card} key={index}>
                <Left style={{flex: 3}}><Text>{item.description}</Text></Left>
                <Right><CheckBox style={{marginRight: 10}} checked={true} onPress={() => this.check(index)}/></Right>
            </View>
        );
    }

    renderAd() {
        if(Store.adVisible) {
            return(
                <AdMobBanner
                    bannerSize="fullBanner"
                    adUnitID="ca-app-pub-3940256099942544/6300978111"
                    testDeviceID="EMULATOR"
                    didFailToReceiveAdWithError={this.bannerError.bind(this)}
                />
            )
        }
    }

    render() {
        return (
            <Root>
                <Content padder>
                    {Store.done.map((item, i) => this.renderListItem(item, i))}
                </Content>
                {this.renderAd()}
            </Root>
        );
    }
}

export default Done;