import React, {Component} from 'react';
import {TouchableOpacity} from 'react-native';
import {Container, Content, Text, Left, Right, CheckBox, Icon, Item, Input} from 'native-base';
import Style from '../Components/Style';
import * as Animatable from 'react-native-animatable';
import {observer} from 'mobx-react/native';
import Store from '../Config/Store';

@observer
class Home extends Component {
    static navigationOptions = {
        tabBarLabel: 'Tasks'
    };

    constructor(props) {
        super(props);
        this.state = {
            footer: false,
            btnAnimation: 'zoomInDown',
            inputAnimation: 'fadeInUp',
            description: ''
        };
    }

    componentWillMount() {
        const initial_data = [{description: '1 Lorem ipsum dolar sit amet'},
            {description: '2 Lorem ipsum dolar'},
            {description: '3 Lorem ipsum dolar'},
            {description: '4 Lorem ipsum dolar'},
            {description: '5 Lorem ipsum dolar'}
        ];
        for (let i = 0; i < initial_data.length; i++) {
            Store.newTask(initial_data[i]);
        }
    }

    check(index) {
        let newDone = Store.tasks.splice(index, 1)[0];
        Store.newDone(newDone);
    }

    addTask() {
        if(this.state.description !== '') {
            let newData = {description: this.state.description, isDone: false};
            Store.newTask(newData);
            this.setState({inputAnimation: 'fadeOutUp', btnAnimation: 'zoomInDown', description: ''});
            setTimeout(() => this.setState({footer: false}), 600);
        } else {
            alert('Type a todo');
        }
    }

    renderFooter() {
        if (this.state.footer) {
            return (
                <Animatable.View animation={this.state.inputAnimation} duration={900}>
                    <Item regular>
                        <Input value={this.state.description} onChangeText={text => this.setState({description: text})} placeholder='Type a todo...'/>
                        <TouchableOpacity onPress={() => this.addTask()}><Icon style={{color: 'green'}} name='add'/></TouchableOpacity>
                        <TouchableOpacity onPress={() => {
                            this.setState({inputAnimation: 'fadeOutUp', btnAnimation: 'zoomInDown'});
                            setTimeout(() => this.setState({footer: false}), 600);
                        }}>
                            <Icon style={{color: 'red', marginRight: 10}} name='close'/>
                        </TouchableOpacity>
                    </Item>
                </Animatable.View>
            );
        } else {
            return (
                <Animatable.View style={Style.floating} animation={this.state.btnAnimation} duration={1100}>
                    <TouchableOpacity style={[Style.floating, {bottom: 0, right: 0}]} onPress={() => {
                        this.setState({btnAnimation: 'zoomOutDown'});
                        setTimeout(() => this.setState({footer: true, inputAnimation: 'fadeInUp'}), 300);
                    }}>
                        <Icon style={{color: '#fff', fontSize: 30}} name="md-add"/>
                    </TouchableOpacity>
                </Animatable.View>
            );
        }
    }

    renderListItem(item, index) {
        return (
            <Animatable.View animation='fadeInDown' duration={1100} style={Style.card} key={index}>
                <Left style={{flex: 3}}><Text>{item.description}</Text></Left>
                <Right><CheckBox style={{marginRight: 10}} checked={false} onPress={() => this.check(index)} /></Right>
            </Animatable.View>
        );
    }

    render() {
        return (
            <Container>
                <Content padder style={{paddingBottom: 50}}>
                    <Animatable.View animation="bounceInUp" duration={1100}>
                        {Store.tasks.map((item, i) => this.renderListItem(item, i))}
                    </Animatable.View>
                </Content>
                {this.renderFooter()}
            </Container>
        );
    }
}

export default Home;